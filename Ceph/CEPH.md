# Understanding CEPH
## I. Overview
Ceph là một mã nguồn mở cung cấp các giải pháp về lưu trữ dữ liệu trên nền tảng phi tập trung. Ceph cho phép phân tách dữ liệu từ các bộ lưu trữ vật lý( Physical Hardware Storage) bằng các lớp trừu tượng phần mềm (Software Abtraction Layers)và cung cấp việc mở rộng khả năng quản lý lỗi(Fault). Một lợi thế của ceph là nó cung cấp giao diện cho nhiều loại storage khác nhau trong cùng một cluster(*RAM*, *SSD*, *HDD*). 
## II. Architecture
![Architecture](images/ceph_architecture.png) <br>
Một hệ sinh thái Ceph bao gồm các Ceph Cluster. Mỗi Ceph Cluster được phát triển dựa trên [RADOS](RADOS.md).<br>
Mỗi Ceph cluster bao gồm hai thành phần nền(daemons)
* Ceph Monitor
* Ceph OSD(Object Storage Device) Deamon 
**Ceph Monitor:** duy trì một bản master copy của các cluster map.
**Ceph OSD Daemon:** kiểm tra trạng thái của chính nó và trạng thái của các OSDs và báo cáo cho Ceph Monitor.<br>

Ceph Client read và write dữ liệu tới các storage clusters. Client cần những thông tin dưới đây để giao tiếp với Ceph Cluster.
* Ceph configuration file, tên cluster(luôn luôn là *ceph*) và địa chỉ monitor.
* Tên pool.
* Usernam và path tới secrete key.
Ceph clients giữ Object IDs và tên Poll(nơi lưu trữ các objects) nhưng nó không cần lưu một object-to-OSD index hay centralized object index để tìm kiếm các vị trí của các object. Để store và retreive dữ liệu, Ceph clients truy cập tới Ceph Motinor và lấy bản copy mới nhất Storage Cluster map. Sau đó Ceph client có thể cung cấp tên object và tên poll và Ceph sẽ sử dung cluster map và giải thuật CRUSH(Controlled Replication Under Scalable Hashing) để tính toán và tìm ra vị trí của object, primary Ceph OSD sẽ lưu trữ hay lấy dữ liệu tương ứng. Ceph clients kết nối tới primary Ceph OSD nơi mà nó có thể thực hiện hành động đọc hay write dữ liệu. Không có một máy chủ trung gian hay broker hoặc bus giữ Ceph client và OSD.
**Storage Cluster Architecture**
Ceph Cluster chứa số lượng lớn các nút Ceph cho khả năng mở rộng vô hạn, hiệu suất và sẵn có cao. Mỗi nút tận dụng phần cứng và Ceph deamons giao tiếp với nhau để:
* Storage và retrive dữ liệu
* Tạo các bản so du liệu
* Theo dõi và báo cáo tình trạng health của cluster.
* Phân phối dữ liệu động
* Đảm bảo tính toàn vẹn của dữ liệu
* Phục hồi lỗi
### 1. Pools
Pools là nơi mà Ceph lưu trữ các data objects. Bạn có thể tạo pool cho mỗi loại dữ liệu cụ thể ví dụ như cho block devices, object gatewayshay đơn giản chỉ là để phân biệt các nhóm user này với các nhóm khác.
Khi Ceph client đọc hay ghi dữ liệu nó luôn luôn kêt nối tới một storage pool trong CSC(Ceph storage cluster). Trên thực tế  pool hoạt động như một phân vùng cục bộ (logical partition) để  điêu khiển việc truy cập các data object.
Trong thực tế, một Ceph Pool không chỉ là một logical partition để lưu trữ data object mà nó còn giữ một vai trò quan trọng trong việc làm thế nào Ceph storage phân phối và lưu trữ data.
**Pool Type**: 

## III. 
## Reference
[1] [Ceph](https://docs.ceph.com/docs/mimic/architecture/#the-ceph-storage-cluster) <br>
[2] [Ceph-RedHat](https://access.redhat.com/documentation/en-us/red_hat_ceph_storage/1.2.3/html-single/red_hat_ceph_architecture/index)