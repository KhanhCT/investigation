# RADOS: A Scalable, Reliable Storage Service for Petabyte-scale Storage Clusters

## I. Introduce Storage Model
Có ba loại storage: Object Storage Model, Block Storage Model, File Storage Model <br>
### 1. Block Storage Model:
Là loại storage tập trung chủ yếu vào giải quyết vấn đề hiệu suất bằng cách cung cấp các khối dữ liệu(block data) có thể kiểm soát được cho các ứng dụng cụ thể thông qua các giao diện khối (Block Interface). Block Storage được sử dụng để lưu trữ dữ liệu trong mạng SAN(Storage Area Network). Trong Block Storage không có mô hình siêu dữ liệu(metadata) cho dữ liệu lưu trữ. Block Storage được sử dụng trong các ứng dụng yêu cầu độ trễ(Latency) thấp, hiệu suất cao(hight performance) và yêu cầu về độ tin cậy dữ liệu(ví dụ: Structured data)<br>
**How to work**: BS(Block Storage) chia dữ liệu ra thành các blocks và luư trữ nó thành những phần riêng biệt và được định danh. <br>
![SNA](images/SAN_blockstorage.png) <br>
BS thường được cấu hình để tách riêng ra khỏi giao diện người dùng và đươc phân tán trên nhiều mỗi trường. BS không dựa vào single path như File Storage. BS tạo multi path cho dữ liệu tức là dữ liệu dược tạo thành nhiều bản sao (replicats) trên các storages để đảm bảo dữ liệu được retrieve nhanh chóng và đảm bảo tính toàn vẹn của dữ liệu.<br>
**Ưu điểm**:
* Độ trễ thấp
* Hiệu suất cao

**Nhược điểm**: 
* Do chủ yếu để lưu trữ các dữ liệu mang tính cấu trúc nên BS có nhiều nhiều giới hạn trong việc xử lý siêu dữ liệu(metadata).
* Dư liệu được tạo thành nhiều bản sao nên gây ra dư thừa dữ liệu.

## 2. File Storage Model
FS(File Storage) là một mô hình mang tính cấp bậc(file-level) và nó có cùng ý tưởng với hệ thống file truyền thống(Traditional Network File System). FS được gắn liếu với công nghệ NAS(Network Attached Storage). 
![NAS](images/NAS_filestorage.png) <br>
**How to work**: 
Người dùng hay các ứng dụng truy cập file thông qua các cây thư mục(Folder Tree) và từng file riêng biệt.
Các file trong FS được gắn mới một single path duy nhất. Do đó hiệu suất của nó sẽ giảm đáng kể khi có multi access. <br>
**Ưu điểm**: 
* Dễ dàng trong việc mở rộng(Highly Scalable)
* Không gây dư thừa dữ liệu
* Dễ quản lý và cấu hình
**Nhược điểm**:
* Hiệu suất thấp
* File có thể được truy cập đồng thời từ nhiều Users nên có lợi thế trong việc chia sẻ data với nhiều Users tại cùng một thời điểm

## 3. Object Storage Model
OS(Object Storage) là mô hình dữ liệu based đối tượng(Object-based Storage). Dữ liệu được chia ra thành các phần (Pieces) được luư trữ trong Single Repository. Mỗi OS bao gồm Object File và metadata liên quan tới object. Do dữ liệu đã được trừu tượng hoá nên OS thích hợp với các dữ liệu phi cấu trúc(image, sound, social media, ...). <br>
**How to work**: 
![](images/filestorage.png)
Mỗi file in object sau khi được lưu vào repository sẽ được định danh bởi một Unique ID. Users hay Application sẽ dùng ID này để định danh. Mỗi OS được lấy thông qua HTTP Application Programming Interface(API).<br>
**Ưu điểm**
* Durable
* Flexible
* High Available
* Scalable
**Nhược điểm**
* Không phù hợp với dữ liệu có cấu trúc.
* 
## 4. Reference
[1] [RADOS: A Scalable, Reliable Storage Service for Petabyte-scale Storage Clusters](https://ceph.com/wp-content/uploads/2016/08/weil-rados-pdsw07.pdf) <br>
[2] [IBM - Storage Model](https://www.ibm.com/cloud/blog/difference-between-bluemix-object-storage-and-other-models) <br> 
[3] [RedHat - Storage Model](https://www.redhat.com/en/topics/data-storage/file-block-object-storage)



