# I. NGINX Config
## I.1 HTTPs
### 1. Gen SSL key
```sh
sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt

sudo openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout /etc/ssl/private/nginx-selfsigned.key -out /etc/ssl/certs/nginx-selfsigned.crt
```
Refer [Generate SSL KEY](https://www.digitalocean.com/community/tutorials/how-to-create-a-self-signed-ssl-certificate-for-nginx-in-ubuntu-16-04)
### 2. NGINX Dockerfile
```sh
cat > Dockerfile << EOF
FROM alpine:3.7

LABEL maintainer="Kostya Esmukov <kostya@esmukov.ru>"

ENV NGINX_VERSION 1.12.2
ENV NGINX_VTS_COMMITISH v0.1.15

RUN GPG_KEYS=B0F4253373F8F6F510D42178520A9993A1C052F8 \
	&& CONFIG="\
		--prefix=/etc/nginx \
		--sbin-path=/usr/sbin/nginx \
		--modules-path=/usr/lib/nginx/modules \
		--conf-path=/etc/nginx/nginx.conf \
		--error-log-path=/var/log/nginx/error.log \
		--http-log-path=/var/log/nginx/access.log \
		--pid-path=/var/run/nginx.pid \
		--lock-path=/var/run/nginx.lock \
		--http-client-body-temp-path=/var/cache/nginx/client_temp \
		--http-proxy-temp-path=/var/cache/nginx/proxy_temp \
		--http-fastcgi-temp-path=/var/cache/nginx/fastcgi_temp \
		--http-uwsgi-temp-path=/var/cache/nginx/uwsgi_temp \
		--http-scgi-temp-path=/var/cache/nginx/scgi_temp \
		--user=nginx \
		--group=nginx \
		--with-http_ssl_module \
		--with-http_realip_module \
		--with-http_addition_module \
		--with-http_sub_module \
		--with-http_dav_module \
		--with-http_flv_module \
		--with-http_mp4_module \
		--with-http_gunzip_module \
		--with-http_gzip_static_module \
		--with-http_random_index_module \
		--with-http_secure_link_module \
		--with-http_stub_status_module \
		--with-http_auth_request_module \
		--with-http_xslt_module=dynamic \
		--with-http_image_filter_module=dynamic \
		--with-http_geoip_module=dynamic \
		--with-threads \
		--with-stream \
		--with-stream_ssl_module \
		--with-stream_ssl_preread_module \
		--with-stream_realip_module \
		--with-stream_geoip_module=dynamic \
		--with-http_slice_module \
		--with-mail \
		--with-mail_ssl_module \
		--with-compat \
		--with-file-aio \
		--with-http_v2_module \
	" \
	&& addgroup -S nginx \
	&& adduser -D -S -h /var/cache/nginx -s /sbin/nologin -G nginx nginx \
	&& apk add --no-cache --virtual .build-deps \
		gcc \
		libc-dev \
		make \
		openssl-dev \
		pcre-dev \
		zlib-dev \
		linux-headers \
		curl \
		gnupg \
		libxslt-dev \
		gd-dev \
		geoip-dev \
	&& curl -fSL https://nginx.org/download/nginx-$NGINX_VERSION.tar.gz -o nginx.tar.gz \
	&& curl -fSL https://nginx.org/download/nginx-$NGINX_VERSION.tar.gz.asc  -o nginx.tar.gz.asc \
	&& curl -fSL https://github.com/vozlt/nginx-module-vts/archive/${NGINX_VTS_COMMITISH}.tar.gz -o nginx-module-vts.tar.gz \
	&& export GNUPGHOME="$(mktemp -d)" \
	&& found=''; \
	for server in \
		ha.pool.sks-keyservers.net \
		hkp://keyserver.ubuntu.com:80 \
		hkp://p80.pool.sks-keyservers.net:80 \
		pgp.mit.edu \
	; do \
		echo "Fetching GPG key $GPG_KEYS from $server"; \
		gpg --keyserver "$server" --keyserver-options timeout=10 --recv-keys "$GPG_KEYS" && found=yes && break; \
	done; \
	test -z "$found" && echo >&2 "error: failed to fetch GPG key $GPG_KEYS" && exit 1; \
	gpg --batch --verify nginx.tar.gz.asc nginx.tar.gz \
	&& rm -rf "$GNUPGHOME" nginx.tar.gz.asc \
	&& mkdir -p /usr/src \
	&& tar -zxC /usr/src -f nginx.tar.gz \
	&& tar -zxC /usr/src -f nginx-module-vts.tar.gz \
	&& NGINX_MODULE_VTS_PATH=`echo /usr/src/nginx-module-vts*` \
	&& rm nginx.tar.gz \
	&& rm nginx-module-vts.tar.gz \
	&& cd /usr/src/nginx-$NGINX_VERSION \
	&& ./configure --add-module="$NGINX_MODULE_VTS_PATH" $CONFIG --with-debug \
	&& make -j$(getconf _NPROCESSORS_ONLN) \
	&& mv objs/nginx objs/nginx-debug \
	&& mv objs/ngx_http_xslt_filter_module.so objs/ngx_http_xslt_filter_module-debug.so \
	&& mv objs/ngx_http_image_filter_module.so objs/ngx_http_image_filter_module-debug.so \
	&& mv objs/ngx_http_geoip_module.so objs/ngx_http_geoip_module-debug.so \
	&& mv objs/ngx_stream_geoip_module.so objs/ngx_stream_geoip_module-debug.so \
	&& ./configure --add-module="$NGINX_MODULE_VTS_PATH" $CONFIG \
	&& make -j$(getconf _NPROCESSORS_ONLN) \
	&& make install \
	&& rm -rf /etc/nginx/html/ \
	&& mkdir /etc/nginx/conf.d/ \
	&& mkdir -p /usr/share/nginx/html/ \
	&& install -m644 html/index.html /usr/share/nginx/html/ \
	&& install -m644 html/50x.html /usr/share/nginx/html/ \
	&& install -m755 objs/nginx-debug /usr/sbin/nginx-debug \
	&& install -m755 objs/ngx_http_xslt_filter_module-debug.so /usr/lib/nginx/modules/ngx_http_xslt_filter_module-debug.so \
	&& install -m755 objs/ngx_http_image_filter_module-debug.so /usr/lib/nginx/modules/ngx_http_image_filter_module-debug.so \
	&& install -m755 objs/ngx_http_geoip_module-debug.so /usr/lib/nginx/modules/ngx_http_geoip_module-debug.so \
	&& install -m755 objs/ngx_stream_geoip_module-debug.so /usr/lib/nginx/modules/ngx_stream_geoip_module-debug.so \
	&& ln -s ../../usr/lib/nginx/modules /etc/nginx/modules \
	&& strip /usr/sbin/nginx* \
	&& strip /usr/lib/nginx/modules/*.so \
	&& rm -rf /usr/src/nginx-$NGINX_VERSION \
	&& rm -rf "$NGINX_MODULE_VTS_PATH" \
	\
	# Bring in gettext so we can get `envsubst`, then throw
	# the rest away. To do this, we need to install `gettext`
	# then move `envsubst` out of the way so `gettext` can
	# be deleted completely, then move `envsubst` back.
	&& apk add --no-cache --virtual .gettext gettext \
	&& mv /usr/bin/envsubst /tmp/ \
	\
	&& runDeps="$( \
		scanelf --needed --nobanner --format '%n#p' /usr/sbin/nginx /usr/lib/nginx/modules/*.so /tmp/envsubst \
			| tr ',' '\n' \
			| sort -u \
			| awk 'system("[ -e /usr/local/lib/" $1 " ]") == 0 { next } { print "so:" $1 }' \
	)" \
	&& apk add --no-cache --virtual .nginx-rundeps $runDeps \
	&& apk del .build-deps \
	&& apk del .gettext \
	&& mv /tmp/envsubst /usr/local/bin/ \
	\
	# Bring in tzdata so users could set the timezones through the environment
	# variables
	&& apk add --no-cache tzdata \
	\
	# forward request and error logs to docker log collector
	&& ln -sf /dev/stdout /var/log/nginx/access.log \
	&& ln -sf /dev/stderr /var/log/nginx/error.log
    
EXPOSE 80

STOPSIGNAL SIGTERM

CMD ["nginx", "-g", "daemon off;"]
EOF
cat >docker-compose.yaml << EOF
version: '0.1'
services:
########################################################
#             NGINX PROXY
########################################################
    validium-nginx:
      container_name: validium-nginx
      image: validium-nginx:0.1
      ports:
        - "80:80"
        - "443:443"
      volumes:
        - "./conf/nginx.conf:/etc/nginx/nginx.conf"
        - "./conf/default:/etc/nginx/sites-enabled/default"
        - "./ssl:/dashboard/ssl"
        - "./upstream:/etc/nginx/upstream:z"
        - "./logs:/var/log/nginx"
EOF
```
### 3. NGINX config files
```sh
cat >nginx.conf << EOF
#user www-data;
# you must set worker processes based on your CPU cores, nginx does not benefit from setting more than that
worker_processes auto; #some last versions calculate it automatically
# number of file descriptors used for nginx
# the limit for the maximum FDs on the server is usually set by the OS.
# if you don't set FD's then OS settings will be used which is by default 2000
worker_rlimit_nofile 200000;

# only log critical errors
#error_log /var/log/nginx/error.log;

pid /run/nginx.pid;

# provides the configuration file context in which the directives that affect connection processing are specified.
events {
    # determines how much clients will be served per worker
    # max clients = worker_connections * worker_processes
    # max clients is also limited by the number of socket connections available on the system (~64k)
    worker_connections 16384;

    # optmized to serve many clients with each thread, essential for linux -- for testing environment
    use epoll;

    # accept as many connections as possible, may flood worker connections if set too low -- for testing environment
    multi_accept on;
}

http {

    server_names_hash_bucket_size 64;
    vhost_traffic_status_zone;
    # cache informations about FDs, frequently accessed files
    # can boost performance, but you need to test those values
    open_file_cache max=200000 inactive=20s; 
    open_file_cache_valid 30s; 
    open_file_cache_min_uses 2;
    open_file_cache_errors on;

    # to boost I/O on HDD we can disable access logs
    access_log off;

	# copies data between one FD and other from within the kernel
    # faster then read() + write()
    sendfile on;

    # send headers in one peace, its better then sending them one by one 
    tcp_nopush on;

    # don't buffer data sent, good for small data bursts in real time
    tcp_nodelay on;

    # reduce the data that needs to be sent over network -- for testing environment
    gzip on;
    gzip_min_length 10240;
    gzip_proxied expired no-cache no-store private auth;
    gzip_types text/plain text/css text/xml text/javascript application/x-javascript application/json application/xml;
    gzip_disable msie6;

 	# allow the server to close connection on non responding client, this will free up memory
    reset_timedout_connection on;

    # request timed out -- default 60
    client_body_timeout 10;

    # if client stop responding, free up memory -- default 60
    send_timeout 2;

    # server will close connection after this time -- default 75
    keepalive_timeout 30;

    # number of requests client can make over keep-alive -- for testing environment
    keepalive_requests 100000;


	types_hash_max_size 2048;

	include /etc/nginx/mime.types;
	default_type application/octet-stream;


	include /etc/nginx/conf.d/*.conf;
	include /etc/nginx/sites-enabled/*;
}
EOF

cat >default_template <<EOF
upstream EU4_SERVER_CLIENT_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_CLIENT_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_SERVICE_MANAGER_PORTAL_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_SERVICE_MANAGER_PORTAL_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_WMPROXY_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_WMPROXY_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_SM_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_SM_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_SM_SOCKETIO {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_SM_SOCKETIO.conf;
}

upstream EU4_SERVER_SM_AUTH {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_SM_AUTH.conf;
}

upstream EU4_SERVER_OAUTH_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_OAUTH_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_MAIL_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_MAIL_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_INSIGHT_EXPLORER_MICRO_SERVICE {
    least_conn;
    keepalive 32;
    include /etc/nginx/upstream/EU4_SERVER_INSIGHT_EXPLORER_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_COINPRICER_MICRO_SERVICE {
        least_conn;
        keepalive 32;
        include /etc/nginx/upstream/EU4_SERVER_COINPRICER_MICRO_SERVICE.conf;
}

upstream EU4_SERVER_AUDFPRINT_MICRO_SERVICE {
        least_conn;
        keepalive 32;
        include /etc/nginx/upstream/EU4_SERVER_AUDFPRINT_MICRO_SERVICE.conf;
}

limit_req_zone $binary_remote_addr zone=mylimit:10m rate=10r/s;

server {
    listen 80;

     location /status {
            vhost_traffic_status_display;
            vhost_traffic_status_display_format html;
            access_log off;
            # allow 127.0.0.1;
            # deny all;
     }
   # return 301 https://$host$request_uri;
}


server {
        listen 443 ssl default_server;
        ssl_certificate /eu4prod/ssl/EU4_DOMAIN_NAME.crt;
        ssl_certificate_key /eu4prod/ssl/EU4_DOMAIN_NAME.key;
        server_name EU4_DOMAIN_NAME;



        # home page
        location / {
                proxy_pass http://EU4_SERVER_CLIENT_MICRO_SERVICE;
                add_header 'Cache-Control' 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
                expires off;
        }

        location /static/ {
                proxy_pass http://EU4_SERVER_CLIENT_MICRO_SERVICE/static/;
                expires 1d;
        }

        location /socket.io {
                root /eu4prod/socket.io;
        }

        # wallet manager service
        location /api/v1/wallet {
                proxy_pass http://EU4_SERVER_WMPROXY_MICRO_SERVICE;
        }

        # wallet manager service
        location /api/v2/wallet {
                proxy_pass http://EU4_SERVER_WMPROXY_MICRO_SERVICE;
        }


        # service stamp image
	location /uploadimage {
		root /eu4prod/eu4-server-sm;
        }

        # stamp manager service
        location /api/v1/sm {
                proxy_pass http://EU4_SERVER_SM_MICRO_SERVICE;
        }

        location /api/v2/sm/accept {
                limit_req zone=mylimit;
                proxy_pass http://EU4_SERVER_SM_MICRO_SERVICE;
        }

        location /api/v2/sm/pay {
                limit_req zone=mylimit;
                proxy_pass http://EU4_SERVER_SM_MICRO_SERVICE;
        }

        location /api/v2/sm {
                proxy_pass http://EU4_SERVER_SM_MICRO_SERVICE;
                add_header 'Cache-Control' 'no-store, no-cache, must-revalidate, proxy-revalidate, max-age=0';
	        expires off;
        }

        location /socketio/v2/sm {
                proxy_pass http://EU4_SERVER_SM_SOCKETIO;
                proxy_http_version 1.1;
                proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
                proxy_set_header Upgrade $http_upgrade;
                proxy_set_header Connection "Upgrade";
	        proxy_set_header Host $host;
        }

        location /auth/google {
                proxy_pass http://EU4_SERVER_SM_AUTH;
        }

        # oauth service
        location /oauth {
                if ($request_method = OPTIONS ) {
                        add_header Access-Control-Allow-Origin $http_origin;
                        add_header Access-Control-Allow-Methods "GET,HEAD,PUT,POST,DELETE,PATCH" always;
                        add_header Access-Control-Allow-Headers "token,pj,role,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range";
                        add_header Access-Control-Max-Age 432000;
                        return 204;
                }
                add_header Access-Control-Allow-Origin $http_origin always;
                add_header Access-Control-Expose-Headers "token,project_id,account_id,action";
                proxy_pass http://EU4_SERVER_OAUTH_MICRO_SERVICE/oauth;
        }

        # mail service
        location /mail {
                if ($request_method = OPTIONS ) {
                        add_header Access-Control-Allow-Origin $http_origin;
                        add_header Access-Control-Allow-Methods "GET,HEAD,PUT,POST,DELETE,PATCH" always;
                        add_header Access-Control-Allow-Headers "token,pj,role,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range";
                        add_header Access-Control-Max-Age 432000;
                        return 204;
                }
                add_header Access-Control-Allow-Origin $http_origin always;
                proxy_pass http://EU4_SERVER_MAIL_MICRO_SERVICE/mail;
        }

        # for insight blockchain explorer page
        location /insight {
                proxy_pass http://EU4_SERVER_INSIGHT_EXPLORER_MICRO_SERVICE/insight;
        }

        location /admin {
                proxy_pass http://EU4_SERVER_SERVICE_MANAGER_PORTAL_MICRO_SERVICE;
        }

        location /admin/static {
                proxy_pass http://EU4_SERVER_SERVICE_MANAGER_PORTAL_MICRO_SERVICE;
        }

        # coinpricer service
        location /coinpricer {
                if ($request_method = OPTIONS ) {
                        add_header Access-Control-Allow-Origin $http_origin;
                        add_header Access-Control-Allow-Methods "GET,HEAD,PUT,POST,DELETE,PATCH" always;
                        add_header Access-Control-Allow-Headers "token,pj,role,DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range";
                        add_header Access-Control-Max-Age 432000;
                        return 204;
                }
                add_header Access-Control-Allow-Origin $http_origin always;
                proxy_pass http://EU4_SERVER_COINPRICER_MICRO_SERVICE;
        }

        # audfprint service
        location /api/v1/ss/s {
                proxy_pass http://EU4_SERVER_AUDFPRINT_MICRO_SERVICE;
        }
        
}

server {
        listen 443 ssl;
        ssl_certificate /eu4prod/ssl/EU4_DOMAIN_NAME.crt;
        ssl_certificate_key /eu4prod/ssl/EU4_DOMAIN_NAME.key;
        server_name EU4_ADMIN_DOMAIN;

        location / {
                proxy_pass http://EU4_SERVER_SERVICE_MANAGER_PORTAL_MICRO_SERVICE;      
        }     
}
EOF
```
## I.2 HTTP
# II. Docker 
## II.1 Install docker
```sh
sudo apt-get update -y
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo apt-key fingerprint 0EBFCD88
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
sudo apt-get update -y
sudo apt-get install docker-ce docker-ce-cli containerd.io -y
```
### Install docker-compose

```sh
VERSION=1.18.0
sudo curl -L https://github.com/docker/compose/releases/download/${VERSION}/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
docker-compose --version
```
### Uninstall docker
```sh
sudo apt-get purge docker-ce
sudo rm -rf /var/lib/docker
```
## II.2 Command
### 1. Inspect 
```sh
sudo docker inspect service_name/container_id
```
### 2. Attach docker container
```sh
sudo docker exec -it service_name/container_id bash
```
### 3. View log container
```sh
sudo docker logs service_name/container_id bash
```
### 4. Run an docker container
```sh
sudo docker run -d --name khanhct-blueprint --hostname khanhct-blueprint -e "SERVICE_5000_NAME=khanhct-blueprint"  -p 0.0.0.0:20002:5000 -v ~/validium_log:/home/ubuntu/validium_log shark.viosoft.com/validium/validium-blueprint:0.1

docker run -itd --name=validium \
      -v ${PWD}:/home/khanhct/workspace/validium-backend \
      --net=host \
      onepill/golang-python
```
### 5. Build Image
```sh

```
## II.2 Application
### MongoDB
```sh
sudo mkdir -p /usr/local/validium/mongo/data
sudo docker rm mongodb
sudo docker run --name mongodb -d -p 0.0.0.0:28017:27017 -v /usr/local/mongo/data:/data/db mongo
```
# III. Libvirsh
## 1. Install
``` sh
yum install qemu-kvm qemu-img virt-manager libvirt libvirt-python libvirt-client virt-install virt-viewer bridge-utils
```
## 2. Create internal network
```sh
cat >internal_network.xml << EOF
<network>
   <name>internal1</name>
   <forward mode='nat'>
      <nat> <port start='1024' end='65535'/>
      </nat>
   </forward>
   <ip address='192.168.123.1' netmask='255.255.255.0'>
   </ip>
</network>
EOF
virsh net-define internal1.xml 
virsh net-autostart internal1
virsh net-start internal1
```
## 2. Create VM
```sh
sudo virt-install \
--virt-type=kvm \
--name ceph-node1 \
--ram 2048 \
--vcpus=2 \
--os-variant=generic \
--virt-type=kvm \
--hvm \
--network bridge:br-external,model=virtio \
--cdrom=/var/lib/libvirt/images/CentOS-7-x86_64-DVD-1611.iso \
--graphics vnc,listen=10.80.200.10,port=5915 \
--disk path=/var/lib/libvirt/images/node1.qcow2,size=20,bus=virtio,format=qcow2 
```
## 3.Restore from existing qcow2 file
```sh
virt-install \
--name vm6 \
--ram 16384 \
--disk path=/var/lib/libvirt/images/vm6.qcow2,size=20 \
--vcpus 8 \
--os-type linux \
--os-variant generic \
--graphics vnc,listen=10.66.88.106,port=5906 \
--console pty,target_type=serial \
--network bridge=br-external \
--network network:internal1,model=virtio \
--network network:internal2,model=virtio \
--import 
```
## 4. Reference
[LIBVISH](https://docs.oracle.com/cd/E19053-01/ldoms.mgr10/820-3838-10/chapter3.html)
# IV. CURL
## 1. GET
```sh
curl -i -H "token:58be294c1617c81a1c05b2ff-5bc84c437ed44a6e42ed73b7-5c063842fe8a75362c83d57b-2700 host: khanhct-blueprint Content-Type: application/json" http://localhost:13001/api/v0.1/vnf?vnf_id=ff8fb7b3454340ec944a8aa83619278f
```
## 2. POST

# WAGON Python
## Build
```sh
wagon create cloudify-libvirt.zip -a '--no-cache-dir -c constraints.txt'
```

# GRUB
```sh  
vi /etc/default/grub
GRUB_CMDLINE_LINUX_DEFAULT="quiet default_hugepagesz=1G hugepagesz=1G hugepages=56 isolcpus=1-60 splash"
grub2-mkconfig -o /boot/grub2/grub.cfg
reboot
```
# DPDK
## Build DPDK
### Mount hugeages
```sh
mkdir -p /mnt/huge
mount -t hugetlbfs nodev /mnt/huge
echo 1024 > /sys/devices/system/node/node1/hugepages/hugepages-2048kB/nr_hugepages
```
### Feroda
```sh
git clone http://dpdk.org/git/dpdk
cd dpdk
git checkout v2.2.0
export RTE_SDK=/path/to/dpdk
export RTE_TARGET=x86_64-native-linuxapp-gcc
vim $DPDK_DIR/config/common_base
	CONFIG_RTE_BUILD_COMBINE_LIBS=y
## Update kernel Header: 
yum -y install kernel-devel kernel-headers
make install T=$RTE_TARGET
sudo modprobe uio_pci_generic
sudo insmod igb_uio.ko
## Run Example l2fwd
cd dpdk/examples/l2fwd
make 
cd build
./l2fwd -l 2-3 -n 4 -- -p 3 -b 0000:00:03.0
```
# MICROSERVICE 
## Registrator/Consul
```sh
docker run -d -p 8500:8500 -p 172.17.0.1:53:8600/udp -p 8400:8400 --name=consul consul agent -server -bootstrap -ui -client=0.0.0.0
```
## API Gateway
```sh
docker run -d --name kong-database \
                -p 9042:9042 \
                cassandra:3

docker run --rm \
    --link kong-database:kong-database \
    -e "KONG_DATABASE=cassandra" \
    -e "KONG_PG_HOST=kong-database" \
    -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
    kong kong migrations up

docker run -d --name kong --dns 172.17.0.1 --dns 8.8.8.8  \
    --link kong-database:kong-database \
    -e "KONG_DATABASE=cassandra" \
    -e "KONG_CASSANDRA_CONTACT_POINTS=kong-database" \
    -e "KONG_PG_HOST=kong-database" \
    -e "KONG_LOG_LEVEL=debug" \
    -e "KONG_DNS_RESOLVER=172.17.0.1" \
    -p 8000:8000 \
    -p 8443:8443 \
    -p 8001:8001 \
    -p 8444:8444 \
    Kong:0.11

docker run -d -p 9080:8080 --name kong-dashboard pgbi/kong-dashboard  start --kong-url  http://kongip:8001
```

### ALL in ONE
```sh
version: '2.1'
services:
#######################################################
#             KONG
########################################################
  kong-database:
    image: postgres:9.5-alpine
    environment:
      - POSTGRES_USER=kong
      - POSTGRES_DB=kong
    healthcheck:
      test: "pg_isready -U kong && psql -d kong -U kong -c \"SELECT 1=1\""
      interval: 10s
      timeout: 5s
      retries: 5

  kong-migration:
    image: kong:${KONG_VERSION}
    depends_on:
      kong-database:
        condition: service_healthy
    environment:
      - KONG_DATABASE=postgres
      - KONG_PG_HOST=kong-database
    command: sh -c "kong migrations up && touch migrations_run && sleep 30"
    healthcheck:
      test: "if [[ -f migrations_run ]] ; then exit 0; else exit 1; fi"
      interval: 10s
      timeout: 5s
      retries: 5

  kong:
    image: kong:${KONG_VERSION}
    depends_on:
      kong-migration:
        condition: service_healthy
    healthcheck:
      test: "kong health"
      interval: 10s
      timeout: 5s
      retries: 5
    environment:
      - KONG_DATABASE=postgres
      - KONG_PG_HOST=kong-database
      - KONG_ADMIN_LISTEN=0.0.0.0:8001
    ports:
      - 8001:8001
      - 8000:8000

  kong-dashboard:
    image: pgbi/kong-dashboard
    ports:
      - 8080:8080
    depends_on:
      kong:
        condition: service_healthy
    command: "start --kong-url http://kong:8001"

#######################################################
#             CONSUL - REGISTRATOR
########################################################
  consul:
    image: consul
    command: "agent -server -bootstrap -ui -client=0.0.0.0"
    ports:
      - 8500:8500

  registrator:
    image: gliderlabs/registrator:latest
    command: "-cleanup -ip ${PRIVATE_IP} -resync 180 consul://${PRIVATE_IP}:8500"
    depends_on:
      - consul
    volumes:
      - /var/run/docker.sock:/tmp/docker.sock

networks:
  default:
    external:
      name: validium
```

# PYTHON CONCRETE
```sh
# https://github.com/Falldog/pyconcrete
git clone https://github.com/Falldog/pyconcrete.git
cd pyconcrete
## install
python setup.py install
## build
python setup.py install --install-lib=/usr/local/lib/python2.7 --install-scripts=./mylib
## Convert .py to .pye
pyconcrete-admin.py compile --source=<your py module dir> --pye
## run
pyconcrete main.pye
## delete file .py:
find . -type f ! -name "*.pye" ! -name "*.conf" ! -name "*.yaml" ! -name "*.yml" ! -name "*.sh" -delete
```
# KUBERNETES
## Setup Python 3.6.4
```sh
sudo apt install build-essential checkinstall
sudo apt install libreadline-gplv2-dev libncursesw5-dev libssl-dev libsqlite3-dev tk-dev libgdbm-dev libc6-dev libbz2-dev
wget https://www.python.org/ftp/python/3.6.4/Python-3.6.4.tar.xz
tar xvf Python-3.6.4.tar.xz
cd Python-3.6.4/
./configure
sudo make install
```
## Export ENVS from file
```sh
set -a
. ./environments.env
set +a
```
## Install PGADMIN 3
[Refer](https://linuxhint.com/install-pgadmin4-ubuntu/)
```sh
sudo apt-get install build-essential libssl-dev libffi-dev libgmp3-dev
virtualenv python-pip libpq-dev python-dev
mkdir pgAdmin4
cd pgAdmin4
virtualenv pgAdmin4
wget https://ftp.postgresql.org/pub/pgadmin/pgadmin4/v4.8/pip/pgadmin4-4.8-py2.py3-none-any.whl
pip install pgadmin4-4.8-py2.py3-none-any.whl

```
### Docker
```
docker pull dpage/pgadmin4
docker run -p 80:80 \
    -e "PGADMIN_DEFAULT_EMAIL=trongkhanh.hust@gmail.com" \
    -e "PGADMIN_DEFAULT_PASSWORD=root" \
    -d dpage/pgadmin4
```

## Redis

```sh
# Connect to remote redis server
redis-cli -h localhost -p 6379
# Get all keys
Keys *

```

#InfluxDB and Grafana
```sh
curl -sL https://repos.influxdata.com/influxdb.key | sudo apt-key add -
source /etc/lsb-release
echo "deb https://repos.influxdata.com/${DISTRIB_ID,,} ${DISTRIB_CODENAME} stable" | sudo tee /etc/apt/sources.list.d/influxdb.list
sudo apt-get update && sudo apt-get install influxdb

## Grafana
echo "deb https://packagecloud.io/grafana/stable/debian/ wheezy main" | sudo tee /etc/apt/sources.list.d/grafana.list
curl https://packagecloud.io/gpg.key | sudo apt-key add -
sudo apt-get update && sudo apt-get install grafana

CLI / SHELL
Start the container:
$ docker run --name=influxdb -d -p 8086:8086 influxdb
Run the influx client in this container:
$ docker exec -it influxdb influx
Or run the influx client in a separate container:
$ docker run --rm --link=influxdb -it influxdb influx -host influxdb
```

## Install K8S
```sh
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
sudo touch /etc/apt/sources.list.d/kubernetes.list 
echo "deb http://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt-get install -y kubectl
```
### Connect to K8S GCP
```sh
gcloud auth login
# Visit Project page to know PROJECT_ID
gcloud config set project PORJECT_ID
gcloud auth list
# Visit K8S Cluster to know this command
gcloud container clusters get-credentials backend --zone asia-east1-a --project sencoinex-devel
kubectl get all --all-namespaces
celery -A celery_worker beat -l info --scheduler django_celery_beat.schedulers:DatabaseScheduler
```

## RABBIT MQ
### Create USER
```sh
rabbitmqctl add_user test test
rabbitmqctl set_user_tags test administrator
rabbitmqctl set_permissions -p / test ".*" ".*" ".*"
```
